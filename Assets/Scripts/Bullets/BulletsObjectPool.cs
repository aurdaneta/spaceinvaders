﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletsObjectPool : MonoBehaviour
{
    public List<GameObject> bullets;
    public List<bool> enableBullets;
    int currentIndex;

    //Delegates
    public delegate void EventGame(int idx);
    public static event EventGame OnShootBullet;

    public delegate void EventEnemyGame(int idx, Vector2 pos);
    public static event EventEnemyGame OnShootEnemyBullet;

    // Start is called before the first frame update
    void Start(){

        enableBullets = new List<bool>();
        for (int i = 0; i < bullets.Count; i++){
            enableBullets.Add(true);
        }

        currentIndex = 0;

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ShootBullet() {

        for (int i = 0; i < enableBullets.Count; i++){
            if (enableBullets[i]){
                if (OnShootBullet != null) OnShootBullet(i);
                enableBullets[i] = false;
                break;
            }
        }
    }

    public void ShootBullet(GameObject enemy) {

        for (int i = 0; i < enableBullets.Count; i++){
            if (enableBullets[i]){
                if (OnShootEnemyBullet != null) OnShootEnemyBullet(i, enemy.transform.position);
                enableBullets[i] = false;
                break;
            }
        }

    }

    public void ResetBullet(int index) {
        enableBullets[index] = true;
    }
}
