﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBullet : Bullet
{
    // Start is called before the first frame update
    protected override void Start(){
        Enemy.OnHit += ResetBullet;
        BulletsObjectPool.OnShootBullet += BulletTraversal;
        base.Start();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    protected override void OnDestroy(){
        Enemy.OnHit -= ResetBullet;
        BulletsObjectPool.OnShootBullet -= BulletTraversal;
    }

    void BulletTraversal(int i){

        if (i == idx){
            transform.position = Player.Instance.transform.position;
            StartCoroutine("UpdateBulletTraversal");
        }
    }

    IEnumerator UpdateBulletTraversal(){

        Vector2 target = new Vector2(transform.position.x, 5.5f);

        while (this.transform.position.y < 5.5f){
            transform.position = Vector2.MoveTowards(transform.position, target, 0.2f);
            yield return null;
        }

        ResetBullet();
    }

    void ResetBullet(){
        this.transform.localPosition = Vector2.zero;
        FireUpReset();
        StopCoroutine("UpdateBulletTraversal");
    }
}
