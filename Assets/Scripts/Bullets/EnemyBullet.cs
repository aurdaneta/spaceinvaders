﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBullet : Bullet
{
    // Start is called before the first frame update
    protected override void Start(){
        BulletsObjectPool.OnShootEnemyBullet += BulletTraversal;
        base.Start();
    }

    protected override void OnDestroy(){
        BulletsObjectPool.OnShootEnemyBullet -= BulletTraversal;
        base.OnDestroy();
    }

    void BulletTraversal(int i, Vector2 position){
        if (i == idx)
        {
            transform.position = position;
            StartCoroutine(UpdateEnemyBulletTraversal(position));
            this.GetComponent<SpriteRenderer>().color = Color.black;
        }
    }

    IEnumerator UpdateEnemyBulletTraversal(Vector2 position){

        Vector2 target = new Vector2(transform.position.x, -5.5f);

        while (this.transform.position.y > -5.5f){
            transform.position = Vector2.MoveTowards(transform.position, target, 0.1f);
            yield return null;
        }

        ResetEnemyBullet();
    }

    void ResetEnemyBullet(){
        this.transform.localPosition = Vector2.zero;
        this.GetComponent<SpriteRenderer>().color = Color.white;
        FireUpReset();
        StopCoroutine("UpdateEnemyBulletTraversal");
    }

}
