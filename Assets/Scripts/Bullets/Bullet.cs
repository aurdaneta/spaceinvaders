﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public int idx;

    // Start is called before the first frame update
    protected virtual void Start(){

        string[] prefixBullet = this.name.Split('_');
        idx = int.Parse(prefixBullet[1]);
    }

    protected virtual void OnDestroy()
    {

    }
    protected void FireUpReset(){
        this.GetComponentInParent<BulletsObjectPool>().ResetBullet(idx);
    }

}
