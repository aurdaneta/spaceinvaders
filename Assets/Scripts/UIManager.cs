﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    public static UIManager Instance;
    public bool isRuning;

    public Text health;
    public Text enemiesKilled;
    public Text finalText;
    public GameObject pausePanel;
    public GameObject finalPanel;
    
    bool isPaused;

    //Delegates
    public delegate void EventGame();
    public static event EventGame InitGame;
    public static event EventGame PauseGame;
    public static event EventGame Resume;
    public static event EventGame Restart;

    // Use this before initialization
    void Awake()
    {
        Instance = this;
        isRuning = false;
    }

    // Start is called before the first frame update
    void Start(){
        isPaused = true;

        Player.OnLoseHP += UpdateHealth;
        Player.OnGameOver += OnGameOver;
        Enemy.OnDead += UpdateEnemiesKilled;
        this.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Escape) || Input.GetKey(KeyCode.P)){
            //isPaused = true;
            OnPause();
        }

    }

    private void OnDestroy(){
        Player.OnLoseHP -= UpdateHealth;
        Player.OnGameOver -= OnGameOver;
        Enemy.OnDead -= UpdateEnemiesKilled;
    }

    public void OnExit() {
        Application.Quit();
    }

    public void OnPause() {
        Time.timeScale = 0.0f;
        pausePanel.SetActive(true);
        isRuning = false;
        /*if (isPaused) Time.timeScale = 0.0f;
        else Time.timeScale = 1.0f;*/
    }

    public void OnInitPlay(){
        isRuning = true;
        if (InitGame != null) InitGame();
    }

    public void OnPlay() {
        isPaused = false;
        OnPause();
    }

    public void OnContinue() {
        isRuning = true;
        Time.timeScale = 1.0f;
        if (Resume != null) Resume();
    }
    public void OnReset() {
        health.text = "3";
        enemiesKilled.text = "0";
        if (Restart != null) Restart();
    }

    public void UpdateHealth() {
        health.text = (int.Parse(health.text) - 1).ToString();
    }

    public void UpdateEnemiesKilled(GameObject enemy){
        enemiesKilled.text = EnemyController.Instance.enemiesKilled.ToString();
    }

    public void OnGameOver() {
        finalText.text = "Game Over";
        finalPanel.SetActive(true);
    }

    public void OnWin(){
        finalText.text = "Win Game";
        finalPanel.SetActive(true);
    }

}
