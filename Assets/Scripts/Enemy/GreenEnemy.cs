﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GreenEnemy : Enemy
{
    //Enemy2

    // Start is called before the first frame update
    protected override void Start()
    {
        type = enemyType.greenEnemy;
        rateAttack = Random.Range(1.0f, 1.8f); //This enemy have little health but it attack is faster
        base.Start();
    }
    
}
