﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum enemyType{
    blueEnemy,
    redEnemy,
    greenEnemy,
    yellowEnemy,
    whiteEnemy,
}

public class Enemy : MonoBehaviour {

    public int hp;
    public int logicPosX;
    public int logicPosY;
    public enemyType type;

    //Attack Settings
    public bool firstLine;
    public float rateAttack;

    //Delegates
    public delegate void EventGame();
    public static event EventGame OnHit;

    public delegate void EventEnemy(GameObject enemy);
    public static event EventEnemy OnDead;
    public static event EventEnemy OnEnemyShoot;

    public delegate void Event(bool direction);
    public static event Event OnFinishMovement;

    private bool flagAttack;

    // Start is called before the first frame update
    protected virtual void Start() {

        flagAttack = false;
        StartCoroutine("InitAttack");
    }

    // Update is called once per frame
    void Update(){
        if (UIManager.Instance.isRuning && EnemyController.Instance.firstLineAttack[logicPosX] == logicPosY && !flagAttack){
            InvokeRepeating("ManageAttack", 1.5f, rateAttack);
            flagAttack = true;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision){
        if (collision.gameObject.tag == "Bullet") {
            hp--;
            if (OnHit != null) OnHit();
            if (hp <= 0) {
                EnemyDead();
                if (OnDead != null) OnDead(this.gameObject);
            }

        }
    }

    IEnumerator InitAttack() {
        while (!UIManager.Instance.isRuning) yield return null;
                
        if (EnemyController.Instance.firstLineAttack[logicPosX] == logicPosY){
            InvokeRepeating("ManageAttack", 1.5f, rateAttack);
            flagAttack = true;
        }
    }

    public void EnemyDead() {
        this.GetComponent<Animator>().SetTrigger("enemyDead");
        EnemyController.Instance.enemiesKilled++;
        CancelInvoke();
    }

    void ManageAttack() {
        EnemyController.Instance.ammo.GetComponent<BulletsObjectPool>().ShootBullet(this.gameObject);
    }
 

}
