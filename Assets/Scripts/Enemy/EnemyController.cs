﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    public static EnemyController Instance;
    public GameObject ammo;
    public GameObject[] enemies;
    public GameObject[][] enemyInstance;
    public int[][] enemyArmy;
    public bool[][] checkBFS;
    public int[] firstLineAttack;
    public int enemiesKilled;

    private bool orientation;

    // Use this before initialization
    void Awake() {
        Instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        //Listeners
        Enemy.OnDead += BFS;
        UIManager.InitGame += InitAttack;
        UIManager.PauseGame += UpdateFlagGame;
        UIManager.Restart += ResetEnemies;
        UIManager.Resume += ResumeDisplacement;

        //Instatiate structures
        enemyArmy = new int[4][];
        enemyInstance = new GameObject[4][];
        checkBFS = new bool[4][];
        firstLineAttack = new int[16];

        for (int i = 0; i < 4; i++){
            enemyArmy[i] = new int[16];
            enemyInstance[i] = new GameObject[16];
            checkBFS[i] = new bool[16];
        }

        InitGame();
        this.gameObject.SetActive(false);
    }
     
    // Update is called once per frame
    void Update(){

    }

    private void OnDestroy(){
        Enemy.OnDead -= BFS;
        UIManager.InitGame -= InitAttack;
        UIManager.PauseGame -= UpdateFlagGame;
        UIManager.Restart -= ResetEnemies;
        UIManager.Resume -= ResumeDisplacement;
    }

    void InitGame() {

        //Init control variables
        orientation = true;
        enemiesKilled = 0;

        //Create random enemy distribution by type - Initialize several control data structures
        for (int j = 0; j < 16; j++){
            firstLineAttack[j] = 3; //Bottom column attack initially
            for (int i = 0; i < 4; i++)
            {
                enemyArmy[i][j] = Random.Range(0, 4); //Exclude white enemy
                checkBFS[i][j] = false;
            }
        }

        SpawnEnemies();

    }

    void UpdateFlagGame() {
        if (UIManager.Instance.isRuning) StartCoroutine(UpdateEnemyPosition());
        else StopAllCoroutines();
    }

    void SpawnEnemies() {
        float xPos = -4.0f;
        float yPos = 2.5f;
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 16; j++) {
                //Instantiate a enemy accord random type
                enemyInstance[i][j] = Instantiate(enemies[enemyArmy[i][j]], new Vector3(xPos, yPos, 0.0f), Quaternion.identity);
                enemyInstance[i][j].transform.SetParent(this.transform);//Group all enemies in a same GameObject
                enemyInstance[i][j].GetComponent<Enemy>().logicPosX = j;//Keep index reference on each instance
                enemyInstance[i][j].GetComponent<Enemy>().logicPosY = i;

                xPos += 0.5f;
            }
            xPos = -4.0f;
            yPos -= 0.5f;
        }

    }

    void InitAttack() {
        this.gameObject.SetActive(true);
        StartCoroutine(UpdateEnemyPosition());
    }

    //Horizontal displace enemy line 
    IEnumerator UpdateEnemyPosition(){

        while (!UIManager.Instance.isRuning) yield return null;

        float stopPosition;
        if (orientation)
        { //rigth direction
            stopPosition = 1.0f;
            while (this.transform.position.x < stopPosition)
            {
                this.transform.position = new Vector2(this.transform.position.x + 0.01f, this.transform.position.y);
                yield return null;
            }
        }
        else
        { //left direction
            stopPosition = -0.5f;
            while (this.transform.position.x > stopPosition)
            {
                this.transform.position = new Vector2(this.transform.position.x - 0.01f, this.transform.position.y);
                yield return null;
            }
        }

        //for (int i = 0; i < 5; i++)
            StartCoroutine("DownDisplacement");

    }

    //Vertical displace enemy line 
    IEnumerator DownDisplacement(){

        //while (!UIManager.Instance.isRuning) yield return null;

        if (UIManager.Instance.isRuning) {
            float stopPosition = this.transform.position.y - 0.5f;
            while (this.transform.position.y > stopPosition)
            {
                this.transform.position = new Vector2(this.transform.position.x, this.transform.position.y - 0.01f);
                yield return null;
            }

            orientation = !orientation;
            StartCoroutine(UpdateEnemyPosition());
        }        
    }

    void BFS(GameObject enemy) {//Breadth First Search Algorithm

        //Keep in a var a init position to BFS
        int x = enemy.GetComponent<Enemy>().logicPosX;
        int y = enemy.GetComponent<Enemy>().logicPosY;

        var checkQueue = new Queue<GameObject>();
        checkQueue.Enqueue(enemy);

        while (checkQueue.Count > 0) {

            GameObject current = checkQueue.Dequeue();

            x = current.GetComponent<Enemy>().logicPosX;
            y = current.GetComponent<Enemy>().logicPosY;


            //Verify neighbors
            if (x + 1 < 16){//Check boundary
                if (!checkBFS[y][x+1] && enemyInstance[y][x+1].GetComponent<Enemy>().type == current.GetComponent<Enemy>().type){
                    checkQueue.Enqueue(enemyInstance[y][x+1]);
                    checkBFS[y][x+1] = true;
                    enemyInstance[y][x+1].GetComponent<Enemy>().EnemyDead();
                }
            }

            if (x - 1 >= 0) {
                if (!checkBFS[y][x-1] && enemyInstance[y][x - 1].GetComponent<Enemy>().type == current.GetComponent<Enemy>().type){
                    checkQueue.Enqueue(enemyInstance[y][x - 1]);
                    checkBFS[y][x - 1] = true;
                    enemyInstance[y][x - 1].GetComponent<Enemy>().EnemyDead();
                }
            }

            if (y + 1 < 4) {
                if (!checkBFS[y+1][x] && enemyInstance[y+1][x].GetComponent<Enemy>().type == current.GetComponent<Enemy>().type){
                    checkQueue.Enqueue(enemyInstance[y + 1][x]);
                    checkBFS[y+1][x] = true;
                    enemyInstance[y+1][x].GetComponent<Enemy>().EnemyDead();
                }
            }

            if (y - 1 >=0) {
                if (!checkBFS[y - 1][x] && enemyInstance[y - 1][x].GetComponent<Enemy>().type == enemy.GetComponent<Enemy>().type){
                    checkQueue.Enqueue(enemyInstance[y - 1][x]);
                    checkBFS[y - 1][x] = true;
                    enemyInstance[y - 1][x].GetComponent<Enemy>().EnemyDead();
                }
            }

        }
        UpdateLineAttack();
    }

    void UpdateLineAttack() {

        //Check which enemies are dead to update first line attack row
        for (int i = 0; i < 16; i++){
            for (int j = 3; j >= 0; j--){
                if (checkBFS[j][i]){
                    if (firstLineAttack[i] >= j){
                        firstLineAttack[i]--;
                    }
                }
            }
        }
    }

    void ResumeDisplacement(){
        StopAllCoroutines();
        StartCoroutine(UpdateEnemyPosition());
    }

    void ResetEnemies() {
        for (int i = 0; i < 4; i++){
            for (int j = 0; j < 16; j++){
                Destroy(enemyInstance[i][j]);
            }
        }

        InitGame();

    }

}
