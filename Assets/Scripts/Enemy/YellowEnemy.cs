﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class YellowEnemy : Enemy
{
    //Enemy3

    // Start is called before the first frame update
    protected override void Start()
    {
        type = enemyType.yellowEnemy;
        rateAttack = Random.Range(2.0f, 2.8f); //This enemy have more health but it attack is slower
        base.Start();
    }
}
