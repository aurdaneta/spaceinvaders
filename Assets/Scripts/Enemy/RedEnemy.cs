﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RedEnemy : Enemy
{
    //Enemy1

    // Start is called before the first frame update
    protected override void Start(){
        type = enemyType.redEnemy;
        rateAttack = Random.Range(2.2f, 3.0f); //This enemy have more health but it attack is slower
        base.Start();
    }
    
}
