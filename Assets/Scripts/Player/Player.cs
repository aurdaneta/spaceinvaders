﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    int hp = 3;

    public static Player Instance;      //Similar behaviour like a Singleton Pattern
    public BulletsObjectPool ammo;

    //Delegates
    public delegate void EventGame();
    public static event EventGame OnLoseHP;
    public static event EventGame OnGameOver;

    // Use this before initialization
    void Awake()
    {
        Instance = this;
    }

    // Start is called before the first frame update
    void Start(){
        UIManager.InitGame += OnEnablePlayer;
        UIManager.Restart += OnEnablePlayer;

    }

    // Update is called once per frame
    void Update(){
        if (UIManager.Instance.isRuning) {
            if (Input.GetKey(KeyCode.LeftArrow))
                this.transform.position = new Vector2(this.transform.position.x - 0.1f, this.transform.position.y);

            if (Input.GetKey(KeyCode.RightArrow))
                this.transform.position = new Vector2(this.transform.position.x + 0.1f, this.transform.position.y);

            if (Input.GetKeyDown(KeyCode.Space))
            {
                ammo.ShootBullet();
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision){
        if (collision.gameObject.tag == "Enemy"){
            hp--;
            if (OnLoseHP != null) OnLoseHP();
        }

        if (hp <= 0) if (OnGameOver != null) OnGameOver();
    }

    void OnEnablePlayer() {
        hp = 3;
        this.transform.position = new Vector3(this.transform.position.x,-2.5f, this.transform.position.z);
    }

}
